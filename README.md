# Utilisation

```python
from happy.core import AppService

app = AppService("testapp.ini")

def on_event(self, event):
    # do something
def on_get_room(self, roomAlias):
    # do something
def on_get_user(self, userId):
    # do something

app.on_event = on_event
app.on_get_room = on_get_room
app.on_get_user = on_get_user

app.start()
```