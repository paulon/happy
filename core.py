import json

import bottle
import configparser
import requests
import sqlalchemy as sql

from bottle import Bottle, template

class AppService:
    def __init__(self, config_file, base):
        self._load_config(config_file)
        self._app = bottle.Bottle()
        self._route()
        self._connect_database()

    def _connect_database():
        self.engine = sql.create_engine(self.conf["database"]["database"], echo=True)
        self.session = sql.orm.sessionmaker(bind=self.engine)


    def _route(self):
        self._app.route("/transactions/<txnId>", ["PUT"], callback=self._put_transaction)
        self._app.route("/users/<userId>", ["GET"], callback=self._get_user)
        self._app.route("/rooms/<roomAlias>", ["GET"], callback=self._get_room)

    def start(self):
        for route in self._app.routes:
            print(route)
        self._app.run(host=self._conf["server"]["host"], port=self._conf["server"]["port"])

    def _load_config(self, config_file):
        self._conf = configparser.ConfigParser()
        self._conf.read(config_file)

    def _http_error(self, error_code: int, error_message: str):
        bottle.response.status = error_code
        return {
            "errcode": f"{'.'.join(self._conf['matrix']['domain'].upper().split('.')[::-1])}.{error_message.upper()}"
        }

    def _check_token(self):
        try:
            token = bottle.request.query.access_token
        except:
            return self._http_error(401, "UNAUTHORIZED")
        if token != self._conf['matrix']['hs_token']:
            return self._http_error(403, "FORBIDDEN")

    def _put_transaction(self, txnId):
        if (check := self._check_token()) is not None:
            return check
        events = bottle.request.json
        for event in events["events"]:
            self.on_event(self, event)
        return {}

    def _get_user(self, userId):
        if (check := self._check_token()) is not None:
            return check
        return self.on_get_user(self, userId)

    def _get_room(roomAlias):
        if (check := self._check_token()) is not None:
            return check
        return self.on_get_room(self, roomAlias)

    # Fonctions à modifier dans l'app service
    def on_event(self, event):
        raise NotImplementedError()
    def on_get_user(self, userId):
        raise NotImplementedError()
    def on_get_room(self, roomAlias):
        raise NotImplementedError()